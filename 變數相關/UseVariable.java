/**
 * 設置變數的範例 code
 * 注意：直接執行此程式會出現錯誤
 */
public class UseVariable{
	public static void main(String[] args) {
		int id;       // 設置一個變數 id ，但沒給默認值；這時候會根據型態給予該型態的默認值，例如 int 的默認值為 0
		int id = 1;   // 設置一個變數 id ，其值為 1
		int i, j;     // 設置兩個變數 i 和 j
		int i = 1, j; // 設置兩個變數 i 和 j， i 設值為 1 但 j 使用默認值
	}
}
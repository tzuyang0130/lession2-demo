public class NPC{
	public void walk(){
		
	}
	public void walk(double speed){
		
	}
	public void eat(Food f){
		
	}
	public boolean isHuman(){
		return true;
	}
	
	public static void main(String[] args){
		NPC n = new NPC();      // 建立一個 NPC Object
		boolean b = n.isHuman() // 使用 n 的 isHuman() method ，因為 isHuman() 回傳類型是 boolean ，所以 b 的類型也是 boolean
		System.out.println(b);  // 輸出 b ，如果程式執行正確，會顯示 true
	}
}
